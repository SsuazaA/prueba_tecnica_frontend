import * as $ from 'jquery';
import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { AppRoutes } from './app.routing';
import { AppComponent } from './app.component';

import { FlexLayoutModule } from '@angular/flex-layout';
import { FullComponent } from './layouts/full/full.component';
import { AppBlankComponent } from './layouts/blank/blank.component';
import { AppHeaderComponent } from './layouts/full/header/header.component';
import { AppSidebarComponent } from './layouts/full/sidebar/sidebar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DemoMaterialModule } from './demo-material-module';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

import { SharedModule } from './shared/shared.module';
import { SpinnerComponent } from './shared/spinner.component';

import { MomentModule } from 'ngx-moment';

import { registerLocaleData } from '@angular/common';
import localeCo from '@angular/common/locales/es-CO';
import localeCoExtra from '@angular/common/locales/extra/es-CO';

import { AgregarPersonaDialogComponent } from './shared/dialogs/agregar-persona-dialog/agregar-persona-dialog.component';
import { ModificarPersonaDialogComponent } from './shared/dialogs/modificar-persona-dialog/modificar-persona-dialog.component';
import { AgregarAreaDialogComponent } from './shared/dialogs/agregar-area-dialog/agregar-area-dialog.component';
import { ModificarAreaDialogComponent } from './shared/dialogs/modificar-area-dialog/modificar-area-dialog.component';

registerLocaleData(localeCo, localeCoExtra);

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true,
  wheelSpeed: 2,
  wheelPropagation: true
};

@NgModule({
  declarations: [
    AppComponent,
    FullComponent,
    AppHeaderComponent,
    SpinnerComponent,
    AppBlankComponent,
    AppSidebarComponent,
    AgregarPersonaDialogComponent,
    ModificarPersonaDialogComponent,
    AgregarAreaDialogComponent,
    ModificarAreaDialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    DemoMaterialModule,
    FormsModule,
    FlexLayoutModule,
    HttpClientModule,
    PerfectScrollbarModule,
    SharedModule,
    MomentModule,
    RouterModule.forRoot(AppRoutes)
  ],
  entryComponents: [
    AgregarPersonaDialogComponent,
    ModificarPersonaDialogComponent,
    AgregarAreaDialogComponent,
    ModificarAreaDialogComponent
  ],
  providers: [
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    },{
      provide: LOCALE_ID,
      useValue: "es-CO"
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
