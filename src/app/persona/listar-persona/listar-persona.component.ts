import { Component, OnInit, ViewChild, EventEmitter, Output, Input } from '@angular/core';

import { Persona } from '../../shared/models/persona.model';
import { Pagina } from '../../shared/models/paged/pagina.model';

import { PersonaService } from '../../shared/services/persona.service';

@Component({
  selector: 'app-listar-persona',
  templateUrl: './listar-persona.component.html',
  styleUrls: ['./listar-persona.component.css']
})
export class ListarPersonaComponent implements OnInit {
	pagina = new Pagina();
  	columns: any[];
  	rows: Persona[];
  	selected = [];
  	sort: any[];

  	loading = true;

  	@Output() selectedValue = new EventEmitter();
    @Input() update: number;
  	@ViewChild(ListarPersonaComponent) table: ListarPersonaComponent;
  	constructor(private personaService: PersonaService) {
  		
  	}

    ngOnChanges() {
      if (this.update > 0) {
        this.obtenerRegistros();
      }
    }

  	ngOnInit() {
  		this.loading = true;

  		this.sort = [{prop: "codigo", dir: "asc"}];

  		this.columns = [
      		{prop: 'codigo', name: 'Codigo', width: 50, sortable: true},
          	{prop: 'identificacion', name: 'Identificación', sortable: true},
      		{prop: 'nombre', name: 'Nombre(s)', sortable: true},
      		{prop: 'apellido', name: 'Apellido(s)', sortable: true}
    	];

    	this.setPage({ offset: 0 });
  	}

  	obtenerRegistros() {
  		this.personaService.obtenerPagina(this.pagina.number, this.sort).subscribe(data => {
      		this.pagina = data;
      		this.rows = data.content;
      		this.loading = false;
    	});
  	}

  	setPage(pageInfo) {
    	this.pagina.number = pageInfo.offset;
    	this.obtenerRegistros();
  	}

  	onSelect({ selected }) {
  		this.selectedValue.emit(this.selected[0]);
  	}

  	onSort(event) {
    	var pagina = 0;
    	this.sort[0].prop = event.sorts[0].prop;
    	this.sort[0].dir = event.sorts[0].dir;
    	
    	this.personaService.obtenerPagina(pagina, this.sort).subscribe(data => {
      		this.pagina = data;
      		this.rows = data.content;
    	});
  	}

}
