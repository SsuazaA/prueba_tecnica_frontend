import { Routes } from '@angular/router';

import { AdministrarPersonaComponent } from './administrar-persona/administrar-persona.component';

export const PersonaRoutes: Routes = [
    {
        path: '',
        children: [
          {
            path: '',
            component: AdministrarPersonaComponent
          }
        ]
    }
];

