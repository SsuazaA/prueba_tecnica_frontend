import 'hammerjs';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { DemoMaterialModule } from '../demo-material-module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { PersonaRoutes } from './persona.routing';

import { AdministrarPersonaComponent } from './administrar-persona/administrar-persona.component';
import { ListarPersonaComponent } from './listar-persona/listar-persona.component';

@NgModule({
  imports: [
    CommonModule,
    	DemoMaterialModule,
    	FormsModule,
    	ReactiveFormsModule,
    	FlexLayoutModule,
      	NgxDatatableModule,
    	RouterModule.forChild(PersonaRoutes)
  ],
  declarations: [AdministrarPersonaComponent, ListarPersonaComponent]
})
export class PersonaModule { 

}
