import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';

import { Persona } from '../../shared/models/persona.model';

import { AgregarPersonaDialogComponent } from '../../shared/dialogs/agregar-persona-dialog/agregar-persona-dialog.component';
import { ModificarPersonaDialogComponent } from '../../shared/dialogs/modificar-persona-dialog/modificar-persona-dialog.component';

@Component({
  selector: 'app-administrar-persona',
  templateUrl: './administrar-persona.component.html',
  styleUrls: ['./administrar-persona.component.css']
})
export class AdministrarPersonaComponent implements OnInit {
  	persona: Persona;
    update: number = 0;

  	constructor(private dialog: MatDialog) {
  		this.persona = new Persona();
  	}

  	ngOnInit() {

  	}

    adicionar() {
      const dialog = this.dialog.open(AgregarPersonaDialogComponent, {
              width: '650px'
        });

        dialog.afterClosed().subscribe(result => {
            if (result != undefined) {
                this.update++;
            }
        });
    }

    modificar() {
      const dialog = this.dialog.open(ModificarPersonaDialogComponent, {
              width: '650px',
              data : this.persona
        });

        dialog.afterClosed().subscribe(result => {
            if (result != undefined) {
                this.update++;
                this.persona.codigo = undefined;
            }
        });
    }

  	obtenerSeleccion(row: Persona) { 
      	this.persona = row;
    }

}
