import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { Persona } from '../../models/persona.model';
import { PersonaService } from '../../services/persona.service';

@Component({
  selector: 'app-modificar-persona-dialog',
  templateUrl: './modificar-persona-dialog.component.html',
  styleUrls: ['./modificar-persona-dialog.component.css']
})
export class ModificarPersonaDialogComponent implements OnInit {
  	submitted = false;
  	step = 0;

  	persona: Persona;

  	constructor(public dialogRef: MatDialogRef<ModificarPersonaDialogComponent>, 
        @Inject(MAT_DIALOG_DATA) public data: Persona, private personaService: PersonaService) {
  		
  	}

  	ngOnInit() {
  		this.persona = new Persona();

      this.persona.codigo = this.data.codigo;
      this.persona.identificacion = this.data.identificacion;
      this.persona.primerNombre = this.data.primerNombre;
      this.persona.segundoNombre = this.data.segundoNombre;
      this.persona.primerApellido = this.data.primerApellido;
      this.persona.segundoApellido = this.data.segundoApellido;
  	}
    
    cancelar() {
        this.dialogRef.close();
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

  	onSubmit() {
        this.submitted = true;
        this.personaService.modificar(this.persona).subscribe(
            data => {
              this.dialogRef.close(this.persona);
              //this.notificacionService.notificar("Agregado con exito");
            }, error => console.log(error)
        );
    }
}
