import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { Area } from '../../models/area.model';
import { Ciudad } from '../../models/ciudad.model';

import { AreaService } from '../../services/area.service';
import { CiudadService } from '../../services/ciudad.service';

@Component({
  selector: 'app-agregar-area-dialog',
  templateUrl: './agregar-area-dialog.component.html',
  styleUrls: ['./agregar-area-dialog.component.css']
})
export class AgregarAreaDialogComponent implements OnInit {
  	submitted = false;
  	step = 0;

  	area: Area;
    ciudades: Ciudad[];

  	constructor(public dialogRef: MatDialogRef<AgregarAreaDialogComponent>, 
        @Inject(MAT_DIALOG_DATA) public data: any, private areaService: AreaService, 
        private ciudadService: CiudadService) {
  	}

  	ngOnInit() {
  		this.area = new Area();

      this.ciudadService.listar().subscribe(data => {
            this.ciudades = data;
          }, error => console.log(error)
      );
  	}
    
    cancelar() {
        this.dialogRef.close();
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

  	onSubmit() {
        this.submitted = true;
        this.areaService.agregar(this.area).subscribe(
            data => {
              this.dialogRef.close(this.area);
              //this.notificacionService.notificar("Agregado con exito");
            }, error => console.log(error)
        );
    }
}
