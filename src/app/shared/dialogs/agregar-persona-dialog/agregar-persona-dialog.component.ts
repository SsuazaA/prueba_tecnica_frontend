import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { Persona } from '../../models/persona.model';
import { PersonaService } from '../../services/persona.service';

@Component({
  selector: 'app-agregar-persona-dialog',
  templateUrl: './agregar-persona-dialog.component.html',
  styleUrls: ['./agregar-persona-dialog.component.css']
})
export class AgregarPersonaDialogComponent implements OnInit {
  	submitted = false;
  	step = 0;

  	persona: Persona;

  	constructor(public dialogRef: MatDialogRef<AgregarPersonaDialogComponent>, 
        @Inject(MAT_DIALOG_DATA) public data: any, private personaService: PersonaService) {
  	}

  	ngOnInit() {
  		this.persona = new Persona();
  	}
    
    cancelar() {
        this.dialogRef.close();
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

  	onSubmit() {
        this.submitted = true;
        this.personaService.agregar(this.persona).subscribe(
            data => {
              this.dialogRef.close(this.persona);
              //this.notificacionService.notificar("Agregado con exito");
            }, error => console.log(error)
        );
    }
}
