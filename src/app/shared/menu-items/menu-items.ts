import { Injectable } from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}
export interface Saperator {
  name: string;
  type?: string;
}
export interface SubChildren {
  state: string;
  name: string;
  type?: string;
}
export interface ChildrenItems {
  state: string;
  name: string;
  type?: string;
  child?: SubChildren[];
}

export interface Menu {
  state: string;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  saperator?: Saperator[];
  children?: ChildrenItems[];
}

const MENUITEMS = [
  {
    state: '',
    name: 'Menu',
    type: 'saperator',
    icon: 'av_timer'
  },
  {
    state: 'activo',
    name: 'Activos',
    type: 'link',
    icon: 'bubble_chart'
  },
  {
    state: 'persona',
    name: 'Personas',
    type: 'link',
    icon: 'bubble_chart'
  },
  {
    state: 'area',
    name: 'Areas',
    type: 'link',
    icon: 'bubble_chart'
  }
];

@Injectable()
export class MenuItems {
  getMenuitem(): Menu[] {
    return MENUITEMS;
  }
}
