import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

import { Constantes } from '../models/constantes.model';
import { TipoActivo } from '../models/tipoActivo.model';
import { Pagina } from '../models/paged/pagina.model';

@Injectable({
    providedIn: 'root'
})
export class TipoActivoService {

    private url: string = "";

    constructor(private http: HttpClient) {
        this.url = this.obtenerUrl();
    }

    obtenerUrl() {
        return Constantes.baseUrl + Constantes.apiTipoActivo;
    }

    buscarPorId(id: number): Observable<TipoActivo> {
        return this.http.get<TipoActivo>(this.url + '/id/ ' + id)
        .pipe(catchError(this.handleError));
    }

    listar(): Observable<TipoActivo[]> {
        return this.http.get<TipoActivo[]>(this.url)
        .pipe(catchError(this.handleError));
    }

    obtenerPagina(pagina: number, sort: any[]): Observable<Pagina<TipoActivo>> {
        return this.http.get<Pagina<TipoActivo>>(this.url + '/paginacion?page=' + pagina + '&sort=' + sort[0].prop + ',' + sort[0].dir)
        .pipe(catchError(this.handleError));
    }

    agregar(tipoActivo: TipoActivo): Observable<TipoActivo> {
        return this.http.post<TipoActivo>(this.url, tipoActivo)
        .pipe(catchError(this.handleError));
    }

    modificar(tipoActivo: TipoActivo): Observable<TipoActivo> {
        return this.http.put<TipoActivo>(this.url, tipoActivo)
        .pipe(catchError(this.handleError));
    }

    activar(id: number): Observable<{}> {
        return this.http.put(this.url + '/' + id + '/activar', null)
        .pipe(catchError(this.handleError));
    }

    inactivar(id: number): Observable<{}> {
        return this.http.put(this.url + '/' + id + '/inactivar', null)
        .pipe(catchError(this.handleError));
    }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            console.error('An error occurred:', error.error.message);
        } else {
            console.error('El servidor ha retornado un error');
        }
        return throwError('Something bad happened; please try again later.');
    };
}
