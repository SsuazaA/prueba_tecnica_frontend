import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

import { Constantes } from '../models/constantes.model';
import { Ciudad } from '../models/ciudad.model';

@Injectable({
    providedIn: 'root'
})
export class CiudadService {

    private url: string = "";

    constructor(private http: HttpClient) {
        this.url = this.obtenerUrl();
    }

    obtenerUrl() {
        return Constantes.baseUrl + Constantes.apiCiudad;
    }

    buscarPorId(id: number): Observable<Ciudad> {
        return this.http.get<Ciudad>(this.url + '/id/ ' + id)
        .pipe(catchError(this.handleError));
    }

    listar(): Observable<Ciudad[]> {
        return this.http.get<Ciudad[]>(this.url)
        .pipe(catchError(this.handleError));
    }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            console.error('An error occurred:', error.error.message);
        } else {
            console.error('El servidor ha retornado un error');
        }
        return throwError('Something bad happened; please try again later.');
    };
}
