import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { DatePipe } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class NotificacionService {

	constructor(private snackBar: MatSnackBar) {

  }

  notificar(mensaje: string) {
    var _mensaje = mensaje + ", " + new DatePipe("es-CO").transform(new Date(), "medium");
    this.snackBar.open(_mensaje, null, {
      horizontalPosition: "center",
      verticalPosition: "bottom",
      duration: 4000
    })
  }
}
