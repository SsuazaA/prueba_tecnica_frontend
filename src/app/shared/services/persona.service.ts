import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

import { Constantes } from '../models/constantes.model';
import { Persona } from '../models/persona.model';
import { Pagina } from '../models/paged/pagina.model';

@Injectable({
    providedIn: 'root'
})
export class PersonaService {

    private url: string = "";

    constructor(private http: HttpClient) {
        this.url = this.obtenerUrl();
    }

    obtenerUrl() {
        return Constantes.baseUrl + Constantes.apiPersona;
    }

    buscarPorId(id: number): Observable<Persona> {
        return this.http.get<Persona>(this.url + '/id/ ' + id)
        .pipe(catchError(this.handleError));
    }

    listar(): Observable<Persona[]> {
        return this.http.get<Persona[]>(this.url)
        .pipe(catchError(this.handleError));
    }

    obtenerPagina(pagina: number, sort: any[]): Observable<Pagina<Persona>> {
        return this.http.get<Pagina<Persona>>(this.url + '/paginacion?page=' + pagina + '&sort=' + sort[0].prop + ',' + sort[0].dir)
        .pipe(catchError(this.handleError));
    }

    agregar(persona: Persona): Observable<Persona> {
        return this.http.post<Persona>(this.url, persona)
        .pipe(catchError(this.handleError));
    }

    modificar(persona: Persona): Observable<Persona> {
        return this.http.put<Persona>(this.url, persona)
        .pipe(catchError(this.handleError));
    }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            console.error('An error occurred:', error.error.message);
        } else {
            console.error('El servidor ha retornado un error');
        }
        return throwError('Something bad happened; please try again later.');
    };
}
