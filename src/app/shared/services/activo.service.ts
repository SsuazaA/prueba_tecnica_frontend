import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

import { Constantes } from '../models/constantes.model';
import { Activo } from '../models/activo.model';
import { Pagina } from '../models/paged/pagina.model';

@Injectable({
    providedIn: 'root'
})
export class ActivoService {

    private url: string = "";

    constructor(private http: HttpClient) {
        this.url = this.obtenerUrl();
    }

    obtenerUrl() {
        return Constantes.baseUrl + Constantes.apiActivo;
    }

    buscarPorId(id: number): Observable<Activo> {
        return this.http.get<Activo>(this.url + '/id/ ' + id)
        .pipe(catchError(this.handleError));
    }

    listar(): Observable<Activo[]> {
        return this.http.get<Activo[]>(this.url)
        .pipe(catchError(this.handleError));
    }

    obtenerPagina(pagina: number, sort: any[]): Observable<Pagina<Activo>> {
        return this.http.get<Pagina<Activo>>(this.url + '/paginacion?page=' + pagina + '&sort=' + sort[0].prop + ',' + sort[0].dir)
        .pipe(catchError(this.handleError));
    }

    buscarPorEstado(estado: number, pagina: number, sort: any[]): Observable<Pagina<Activo>> {
        return this.http.get<Pagina<Activo>>(this.url + '/estado/' + estado + '?page=' + pagina + '&sort=' + sort[0].prop + ',' + sort[0].dir)
        .pipe(catchError(this.handleError));
    }

    agregar(activo: Activo): Observable<Activo> {
        return this.http.post<Activo>(this.url, activo)
        .pipe(catchError(this.handleError));
    }

    modificar(activo: Activo): Observable<Activo> {
        return this.http.put<Activo>(this.url, activo)
        .pipe(catchError(this.handleError));
    }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            console.error('An error occurred:', error.error.message);
        } else {
            console.error('El servidor ha retornado un error');
        }
        return throwError('Something bad happened; please try again later.');
    };
}
