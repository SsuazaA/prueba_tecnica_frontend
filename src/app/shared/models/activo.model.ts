import { TipoActivo } from './tipoActivo.model';
import { EstadoActivo } from './estadoActivo.model';
import { ActivoAsignado } from './activoAsignado.model';

export class Activo {
  	codigo: number;
  	nombre: string;
  	descripcion: string;
  	tipoActivo: TipoActivo;
  	serial: string;
  	numeroInventario: string;
  	perso: number;
  	alto: number;
  	ancho: number;
  	largo: number;
  	valorCompra: string;
  	fechaCompra: Date;
  	fechaBaja: Date;
  	estado: EstadoActivo;
  	color: string;
  	asignado: ActivoAsignado;
}