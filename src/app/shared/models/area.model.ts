import { Ciudad } from './ciudad.model';

export class Area {
  	codigo: number;
  	nombre: string;
  	ciudad: Ciudad;
  	direccion: string;
}