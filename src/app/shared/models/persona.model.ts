export class Persona {
  	codigo: number;
  	identificacion: string;
  	primerNombre: string;
  	segundoNombre: string;
  	primerApellido: string;
  	segundoApellido: string;
}