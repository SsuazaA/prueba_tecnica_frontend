export class Pagina<T> {
    content = new Array<T>();
    totalElements: number = 0;
    totalPages: number = 0;
    last: boolean = false;
    size: number = 0;
    number: number = 0;
}