import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';

import { TipoAsignacion } from './tipoAsignacion.model';

@Injectable({
  	providedIn: 'root'
})
export class Constantes {

    public static httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
    public static baseUrl: string = "http://localhost:8082/prueba/api/";

    public static apiActivo: string = "activo";
    public static apiArea: string = "area";
    public static apiCiudad: string = "ciudad";
    public static apiEstadoActivo: string = "activo/estado";
    public static apiPersona: string = "persona";
    public static apiTipoActivo: string = "activo/tipo";

    public static tipoAsignacion: TipoAsignacion[] = [{
        codigo: 1,
        nombre: "Persona"
    }, {
        codigo: 2,
        nombre: "Area"
    }];

    constructor() {
        
    };
}