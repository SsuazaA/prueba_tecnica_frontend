import { Activo } from './activo.model';
import { Persona } from './persona.model';
import { Area } from './area.model';

export class ActivoAsignado {
  	codigo: number;
  	activo: Activo;
  	persona: Persona;
  	area: Area;
  	estado: number;
}