import { Routes } from '@angular/router';

import { AdministrarAreaComponent } from './administrar-area/administrar-area.component';

export const AreaRoutes: Routes = [
    {
        path: '',
        children: [
          {
            path: '',
            component: AdministrarAreaComponent
          }
        ]
    }
];

