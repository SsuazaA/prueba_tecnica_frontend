import 'hammerjs';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { DemoMaterialModule } from '../demo-material-module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { AreaRoutes } from './area.routing';

import { AdministrarAreaComponent } from './administrar-area/administrar-area.component';
import { ListarAreaComponent } from './listar-area/listar-area.component';

@NgModule({
  imports: [
    CommonModule,
    	DemoMaterialModule,
    	FormsModule,
    	ReactiveFormsModule,
    	FlexLayoutModule,
      	NgxDatatableModule,
    	RouterModule.forChild(AreaRoutes)
  ],
  declarations: [AdministrarAreaComponent, ListarAreaComponent]
})
export class AreaModule { 

}
