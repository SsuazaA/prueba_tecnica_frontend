import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministrarAreaComponent } from './administrar-area.component';

describe('AdministrarAreaComponent', () => {
  let component: AdministrarAreaComponent;
  let fixture: ComponentFixture<AdministrarAreaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdministrarAreaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministrarAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
