import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';

import { Area } from '../../shared/models/area.model';

import { AgregarAreaDialogComponent } from '../../shared/dialogs/agregar-area-dialog/agregar-area-dialog.component';
import { ModificarAreaDialogComponent } from '../../shared/dialogs/modificar-area-dialog/modificar-area-dialog.component';

@Component({
  selector: 'app-administrar-area',
  templateUrl: './administrar-area.component.html',
  styleUrls: ['./administrar-area.component.css']
})
export class AdministrarAreaComponent implements OnInit {
  	area: Area;
    update: number = 0;

  	constructor(private dialog: MatDialog) {
  		this.area = new Area();
  	}

  	ngOnInit() {

  	}

    adicionar() {
      const dialog = this.dialog.open(AgregarAreaDialogComponent, {
              width: '550px'
        });

        dialog.afterClosed().subscribe(result => {
            if (result != undefined) {
                this.update++;
            }
        });
    }

    modificar() {
      const dialog = this.dialog.open(ModificarAreaDialogComponent, {
              width: '550px',
              data : this.area
        });

        dialog.afterClosed().subscribe(result => {
            if (result != undefined) {
                this.update++;
                this.area.codigo = undefined;
            }
        });
    }

  	obtenerSeleccion(row: Area) { 
      	this.area = row;
    }

}
