import { Component, OnInit, ViewChild, EventEmitter, Output, Input } from '@angular/core';

import { Area } from '../../shared/models/area.model';
import { Pagina } from '../../shared/models/paged/pagina.model';

import { AreaService } from '../../shared/services/area.service';

@Component({
  selector: 'app-listar-area',
  templateUrl: './listar-area.component.html',
  styleUrls: ['./listar-area.component.css']
})
export class ListarAreaComponent implements OnInit {
	pagina = new Pagina();
  	columns: any[];
  	rows: Area[];
  	selected = [];
  	sort: any[];

  	loading = true;

  	@Output() selectedValue = new EventEmitter();
    @Input() update: number;
  	@ViewChild(ListarAreaComponent) table: ListarAreaComponent;
  	constructor(private areaService: AreaService) {
  		
  	}

    ngOnChanges() {
      if (this.update > 0) {
        this.obtenerRegistros();
      }
    }

  	ngOnInit() {
  		this.loading = true;

  		this.sort = [{prop: "codigo", dir: "asc"}];

  		this.columns = [
      		{prop: 'codigo', name: 'Codigo', width: 50, sortable: true},
          {prop: 'nombre', name: 'Nombre', sortable: true},
      		{prop: 'ciudad.nombre', name: 'Ciudad', sortable: true},
      		{prop: 'direccion', name: 'Dirección', sortable: true}
    	];

    	this.setPage({ offset: 0 });
  	}

  	obtenerRegistros() {
  		this.areaService.obtenerPagina(this.pagina.number, this.sort).subscribe(data => {
      		this.pagina = data;
      		this.rows = data.content;
      		this.loading = false;
    	});
  	}

  	setPage(pageInfo) {
    	this.pagina.number = pageInfo.offset;
    	this.obtenerRegistros();
  	}

  	onSelect({ selected }) {
  		this.selectedValue.emit(this.selected[0]);
  	}

  	onSort(event) {
    	var pagina = 0;
    	this.sort[0].prop = event.sorts[0].prop;
    	this.sort[0].dir = event.sorts[0].dir;
    	
    	this.areaService.obtenerPagina(pagina, this.sort).subscribe(data => {
      		this.pagina = data;
      		this.rows = data.content;
    	});
  	}

}
