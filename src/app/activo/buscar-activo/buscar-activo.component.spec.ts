import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuscarActivoComponent } from './buscar-activo.component';

describe('BuscarActivoComponent', () => {
  let component: BuscarActivoComponent;
  let fixture: ComponentFixture<BuscarActivoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuscarActivoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuscarActivoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
