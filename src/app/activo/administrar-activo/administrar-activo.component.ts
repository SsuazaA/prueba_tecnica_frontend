import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';

import { Activo } from '../../shared/models/activo.model';

import { AgregarPersonaDialogComponent } from '../../shared/dialogs/agregar-persona-dialog/agregar-persona-dialog.component';
import { ModificarPersonaDialogComponent } from '../../shared/dialogs/modificar-persona-dialog/modificar-persona-dialog.component';

@Component({
  selector: 'app-administrar-activo',
  templateUrl: './administrar-activo.component.html',
  styleUrls: ['./administrar-activo.component.css']
})
export class AdministrarActivoComponent implements OnInit {
  	activo: Activo;
    update: number = 0;

  	constructor(private dialog: MatDialog) {
  		this.activo = new Activo();
  	}

  	ngOnInit() {

  	}

    adicionar() {
      const dialog = this.dialog.open(AgregarPersonaDialogComponent, {
              width: '650px'
        });

        dialog.afterClosed().subscribe(result => {
            if (result != undefined) {
                this.update++;
            }
        });
    }

    modificar() {
      const dialog = this.dialog.open(ModificarPersonaDialogComponent, {
              width: '650px',
              data : this.activo
        });

        dialog.afterClosed().subscribe(result => {
            if (result != undefined) {
                this.update++;
                this.activo.codigo = undefined;
            }
        });
    }

  	obtenerSeleccion(row: Activo) { 
      	this.activo = row;
    }

}
