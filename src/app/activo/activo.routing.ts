import { Routes } from '@angular/router';

import { AdministrarActivoComponent } from './administrar-activo/administrar-activo.component';
import { AgregarActivoComponent } from './agregar-activo/agregar-activo.component';
import { ModificarActivoComponent } from './modificar-activo/modificar-activo.component';
import { BuscarActivoComponent } from './buscar-activo/buscar-activo.component';

export const ActivoRoutes: Routes = [
    {
        path: '',
        children: [
          	{
            	path: '',
            	component: AdministrarActivoComponent
          	}, {
        		path: 'buscar',
        		component: BuscarActivoComponent
      		}, {
        		path: 'agregar',
        		component: AgregarActivoComponent
      		}, {
            	path: 'modificar/:codigo',
            	component: ModificarActivoComponent
          	}
        ]
    }
];

