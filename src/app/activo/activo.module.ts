import 'hammerjs';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { DemoMaterialModule } from '../demo-material-module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { ActivoRoutes } from './activo.routing';

import { AdministrarActivoComponent } from './administrar-activo/administrar-activo.component';
import { ListarActivoComponent } from './listar-activo/listar-activo.component';
import { AgregarActivoComponent } from './agregar-activo/agregar-activo.component';
import { ModificarActivoComponent } from './modificar-activo/modificar-activo.component';
import { BuscarActivoComponent } from './buscar-activo/buscar-activo.component';

@NgModule({
  imports: [
    CommonModule,
    	DemoMaterialModule,
    	FormsModule,
    	ReactiveFormsModule,
    	FlexLayoutModule,
      	NgxDatatableModule,
    	RouterModule.forChild(ActivoRoutes)
  ],
  declarations: [AdministrarActivoComponent, ListarActivoComponent, AgregarActivoComponent, ModificarActivoComponent, BuscarActivoComponent]
})
export class ActivoModule { 

}
