import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarActivoComponent } from './listar-activo.component';

describe('ListarActivoComponent', () => {
  let component: ListarActivoComponent;
  let fixture: ComponentFixture<ListarActivoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListarActivoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarActivoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
