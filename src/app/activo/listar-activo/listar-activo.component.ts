import { Component, OnInit, ViewChild, EventEmitter, Output, Input } from '@angular/core';

import { Activo } from '../../shared/models/activo.model';
import { EstadoActivo } from '../../shared/models/estadoActivo.model';
import { Pagina } from '../../shared/models/paged/pagina.model';

import { ActivoService } from '../../shared/services/activo.service';
import { EstadoActivoService } from '../../shared/services/estadoActivo.service';

@Component({
  selector: 'app-listar-activo',
  templateUrl: './listar-activo.component.html',
  styleUrls: ['./listar-activo.component.css']
})
export class ListarActivoComponent implements OnInit {
    estadosActivos: EstadoActivo[];
    estadoActivo: EstadoActivo;

	  pagina = new Pagina();
  	columns: any[];
  	rows: Activo[];
  	selected = [];
  	sort: any[];

  	loading = true;

  	@Output() selectedValue = new EventEmitter();
    @Input() update: number;
  	@ViewChild(ListarActivoComponent) table: ListarActivoComponent;
  	constructor(private activoService: ActivoService, private estadoActivoService: EstadoActivoService) {
  		 
  	}

    ngOnChanges() {
      if (this.update > 0) {
        this.obtenerRegistros();
      }
    }

  	ngOnInit() {
      this.estadoActivo = new EstadoActivo();
      this.estadoActivo.codigo = 0;

  		this.loading = true;

  		this.sort = [{prop: "codigo", dir: "asc"}];

  		this.columns = [
      		{prop: 'codigo', name: 'Codigo', width: 50, sortable: true},
          {prop: 'serial', name: 'Serial', sortable: true},
      		{prop: 'nombre', name: 'Nombre', sortable: true},
      		{prop: 'tipoActivo.nombre', name: 'Tipo', sortable: true},
          {prop: 'estado.nombre', name: 'Estado', sortable: true},
          {prop: 'asignado', name: 'Asignado', sortable: true}
    	];

    	this.setPage({ offset: 0 });

      this.estadosActivos = [];

      this.estadoActivoService.listar().subscribe(data => {
          var estado = new EstadoActivo();
          estado.codigo = 0;
          estado.nombre = "Todos";
          this.estadosActivos.push(estado);

          for (var i = 0; i < data.length; i++) {
            this.estadosActivos.push(data[i]);
          }
      });
  	}

    buscarPorEstado(event) {
        if (event.value != undefined) {
            this.obtenerRegistros();
        }
    };

  	obtenerRegistros() {
      if (this.estadoActivo.codigo != 0) {
        this.activoService.buscarPorEstado(this.estadoActivo.codigo, this.pagina.number, this.sort).subscribe(data => {
            this.pagina = data;
            this.rows = data.content;
            this.loading = false;
        });
      } else {
        this.activoService.obtenerPagina(this.pagina.number, this.sort).subscribe(data => {
            this.pagina = data;
            this.rows = data.content;
            this.loading = false;
        });
      }
  	}

  	setPage(pageInfo) {
    	this.pagina.number = pageInfo.offset;
    	this.obtenerRegistros();
  	}

  	onSelect({ selected }) {
  		this.selectedValue.emit(this.selected[0]);
  	}

  	onSort(event) {
    	var pagina = 0;
    	this.sort[0].prop = event.sorts[0].prop;
    	this.sort[0].dir = event.sorts[0].dir;
    	
      if (this.estadoActivo.codigo != 0) {
        this.activoService.buscarPorEstado(this.estadoActivo.codigo, pagina, this.sort).subscribe(data => {
            this.pagina = data;
            this.rows = data.content;
        });
      } else {
        this.activoService.obtenerPagina(pagina, this.sort).subscribe(data => {
            this.pagina = data;
            this.rows = data.content;
        });
      }
  	}

}
