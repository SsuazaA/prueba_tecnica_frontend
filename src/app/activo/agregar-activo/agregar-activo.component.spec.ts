import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgregarActivoComponent } from './agregar-activo.component';

describe('AgregarActivoComponent', () => {
  let component: AgregarActivoComponent;
  let fixture: ComponentFixture<AgregarActivoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgregarActivoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgregarActivoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
