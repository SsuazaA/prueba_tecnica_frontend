import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import * as moment from 'moment';

import { Constantes } from '../../shared/models/constantes.model';
import { Activo } from '../../shared/models/activo.model';
import { ActivoAsignado } from '../../shared/models/activoAsignado.model';
import { TipoActivo } from '../../shared/models/tipoActivo.model';
import { EstadoActivo } from '../../shared/models/estadoActivo.model';
import { Area } from '../../shared/models/area.model';
import { Persona } from '../../shared/models/persona.model';

import { ActivoService } from '../../shared/services/activo.service';
import { TipoActivoService } from '../../shared/services/tipoActivo.service';
import { EstadoActivoService } from '../../shared/services/estadoActivo.service';
import { AreaService } from '../../shared/services/area.service';
import { PersonaService } from '../../shared/services/persona.service';
import { NotificacionService } from '../../shared/services/notificacion.service';

@Component({
  selector: 'app-agregar-activo',
  templateUrl: './agregar-activo.component.html',
  styleUrls: ['./agregar-activo.component.css']
})
export class AgregarActivoComponent implements OnInit {
	  submitted = false;
  	step = 0;

  	activo: Activo;
  	tiposActivos: TipoActivo[];
  	estadosActivos: EstadoActivo[];
  	activoAsignado: ActivoAsignado;
  	areas: Area[];
  	personas: Persona[];
  	persona: boolean = false;
  	area: boolean = false;
  	tipoAsignacion : any[] = Constantes.tipoAsignacion;

  	constructor(private location: Location, private router: Router, private activoService: ActivoService, 
  		private tipoActivoService: TipoActivoService, private estadoActivoService: EstadoActivoService, 
  		private areaService: AreaService, private personaService: PersonaService, 
      private notificacionService: NotificacionService) {
  		this.activo = new Activo();
  		this.activoAsignado = new ActivoAsignado();
  		this.activoAsignado.estado = 1;
  		this.activo.asignado = this.activoAsignado;
  		this.activo.estado = new EstadoActivo();
  	}

  	ngOnInit() {
  		this.tipoActivoService.listar().subscribe(data => {
              this.tiposActivos = data;
            }, error => console.log(error)
        );

        this.estadoActivoService.listar().subscribe(data => {
              this.estadosActivos = data;
            }, error => console.log(error)
        );

        this.estadoActivoService.listar().subscribe(data => {
              this.estadosActivos = data;
            }, error => console.log(error)
        );

        this.personaService.listar().subscribe(
            data => this.personas = data, error => console.log(error)
        );

        this.areaService.listar().subscribe(
            data => this.areas = data, error => console.log(error)
        );
  	}

  	seleccion(event) {
        if (event.value != undefined) {
            if (event.value == 1) {
                this.persona = true;
                this.area = false;
                this.activo.asignado.area = null;
            } else {
                this.persona = false;
                this.area = true;
                this.activo.asignado.persona = null;
            }
        }
    };

  	volver() {
      this.location.back();
    }

  	onSubmit() {
        this.submitted = true;
        this.activoService.agregar(this.activo).subscribe(
            data => {
              this.notificacionService.notificar("Agregado con exito");
              this.router.navigate(['/activo']);
            }, error => console.log(error)
        );
    }

}
