import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import * as moment from 'moment';

import { Constantes } from '../../shared/models/constantes.model';
import { Activo } from '../../shared/models/activo.model';
import { ActivoAsignado } from '../../shared/models/activoAsignado.model';
import { TipoActivo } from '../../shared/models/tipoActivo.model';
import { EstadoActivo } from '../../shared/models/estadoActivo.model';
import { Area } from '../../shared/models/area.model';
import { Persona } from '../../shared/models/persona.model';
import { TipoAsignacion } from '../../shared/models/tipoAsignacion.model';

import { ActivoService } from '../../shared/services/activo.service';
import { TipoActivoService } from '../../shared/services/tipoActivo.service';
import { EstadoActivoService } from '../../shared/services/estadoActivo.service';
import { AreaService } from '../../shared/services/area.service';
import { PersonaService } from '../../shared/services/persona.service';
import { NotificacionService } from '../../shared/services/notificacion.service';

@Component({
  selector: 'app-modificar-activo',
  templateUrl: './modificar-activo.component.html',
  styleUrls: ['./modificar-activo.component.css']
})
export class ModificarActivoComponent implements OnInit {
	  submitted = false;
  	step = 0;
  	codigo: string;

  	activo: Activo;
  	tiposActivos: TipoActivo[];
  	estadosActivos: EstadoActivo[];
  	activoAsignado: ActivoAsignado;
  	areas: Area[];
  	personas: Persona[];
  	persona: boolean = false;
  	area: boolean = false;
  	tipo: TipoAsignacion;
  	tipoAsignacion : TipoAsignacion[] = Constantes.tipoAsignacion;

  	constructor(private location: Location, private router: Router, private route: ActivatedRoute,
  		private activoService: ActivoService, private tipoActivoService: TipoActivoService, 
  		private estadoActivoService: EstadoActivoService, private areaService: AreaService, 
  		private personaService: PersonaService, private notificacionService: NotificacionService) {
  		this.activo = new Activo();
  		this.activoAsignado = new ActivoAsignado();
  		this.activoAsignado.estado = 1;
  		this.activo.asignado = this.activoAsignado;
  		this.activo.estado = new EstadoActivo();

  		this.tipo = new TipoAsignacion();
  	}

  	ngOnInit() {
  		this.codigo = this.route.snapshot.params['codigo'];

  		if (this.codigo == undefined || this.codigo == null) {
          	this.router.navigate(['/activo']);
      	} else {
      		this.activoService.buscarPorId(+this.codigo).subscribe(data => {
      			this.activo = data
      			this.activo.fechaCompra = new Date(this.activo.fechaCompra);
      			if (data.asignado.persona != null) {
      				this.tipo = Constantes.tipoAsignacion[0];
      				this.persona = true;
                	this.area = false;
      			} else {
      				this.tipo = Constantes.tipoAsignacion[1];
      				this.persona = false;
                	this.area = true;
      			}
      		}, error => console.log(error));
      	}

  		this.tipoActivoService.listar().subscribe(data => {
              this.tiposActivos = data;
            }, error => console.log(error)
        );

        this.estadoActivoService.listar().subscribe(data => {
              this.estadosActivos = data;
            }, error => console.log(error)
        );

        this.estadoActivoService.listar().subscribe(data => {
              this.estadosActivos = data;
            }, error => console.log(error)
        );

        this.personaService.listar().subscribe(
            data => this.personas = data, error => console.log(error)
        );

        this.areaService.listar().subscribe(
            data => this.areas = data, error => console.log(error)
        );
  	}

  	compareFn(c1: any, c2:any): boolean {     
		return c1 && c2 ? c1.codigo === c2.codigo : c1 === c2; 
	}

  	seleccion(event) {
        if (event.value != undefined) {
            if (event.value == 1) {
                this.persona = true;
                this.area = false;
                this.activo.asignado.area = null;
            } else {
                this.persona = false;
                this.area = true;
                this.activo.asignado.persona = null;
            }
        }
    };

  	volver() {
      this.location.back();
    }

    compararFechas(fechaBaja: Date, fechaCompra: Date) {
        var momentA = moment(fechaBaja,"DD/MM/YYYY");
        var momentB = moment(fechaCompra,"DD/MM/YYYY");
        if (momentA > momentB) return 1;
        else if (momentA < momentB) return -1;
        else return 0;
    }

    validar() {
      if (this.activo.estado.codigo == 2) {
        var resultado = this.compararFechas(this.activo.fechaBaja, this.activo.fechaCompra);
        if (resultado > 0) {
          return true;
        } else {
          return false;
        }
      } else {
        return true;
      }
    }

  	onSubmit() {
        if (this.validar()) {
          this.submitted = true;
          this.activoService.modificar(this.activo).subscribe(
              data => {
                this.notificacionService.notificar("Modificado con exito");
                this.router.navigate(['/activo']);
              }, error => console.log(error)
          );
        } else {
          this.notificacionService.notificar("La fecha de baja no puede ser mayor a la fecha de compra");
        } 
    }

}
