import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModificarActivoComponent } from './modificar-activo.component';

describe('ModificarActivoComponent', () => {
  let component: ModificarActivoComponent;
  let fixture: ComponentFixture<ModificarActivoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModificarActivoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModificarActivoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
